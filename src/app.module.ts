import { Module } from '@nestjs/common';
import { AuthModule } from './modules/auth/auth.module';
import { AccountModule } from './modules/account/account.module';
import { DotenvModule } from './modules/dotenv/dotenv.module';

@Module({
  imports: [
    AuthModule,
    AccountModule,
    DotenvModule,
  ],
})
export class AppModule {}
