import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AuthModule } from '../../modules/auth/auth.module';
import { DemoAccountHelperFixture } from '../helpers/fixtures/demo-account.helper-fixture';
import { E2eHelpersService } from '../helpers/e2e-helpers.service';
import { E2eHelpersModule } from '../helpers/e2e-helpers.module';

describe('auth.e2e-spec.ts', () => {
  let app: INestApplication;
  let e2eHelpers: E2eHelpersService;

  beforeAll(async() => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        E2eHelpersModule,
        AuthModule,
      ],
    }).compile();

    e2eHelpers = module.get(E2eHelpersService);

    app = module.createNestApplication();

    await app.init();
  });

  beforeAll(async() => {
    await e2eHelpers.setUpTestingDatabase([
      DemoAccountHelperFixture,
    ]);
  });

  afterAll(async() => {
    await e2eHelpers.cleanUpTestingDatabase();

    await app.close();
  });

  it('/POST login: login correctly for user "foo"', async() => {
    return request(app.getHttpServer())
      .post('/login')
      .send({ username: 'foo', password: '123456' })
      .set('Accept', 'application/json')
      .expect(200)
      .expect('Content-Type', /json/)
      .expect({
        success: true,
      })
    ;
  });

  it('/POST login: user "foo" will be found, but credentials are invalid', async() => {
    return request(app.getHttpServer())
      .post('/login')
      .send({ username: 'foo', password: '1234567' })
      .set('Accept', 'application/json')
      .expect(403)
      .expect('Content-Type', /json/)
      .expect({
        success: false,
        status: 403,
        message: 'Invalid credentials',
      })
    ;
  });

  it('/POST login: user "baz" will not be found', async() => {
    return request(app.getHttpServer())
      .post('/login')
      .send({ username: 'baz', password: '123456' })
      .set('Accept', 'application/json')
      .expect(404)
      .expect('Content-Type', /json/)
      .expect({
        success: false,
        status: 404,
        message: 'Account with given username was not found',
      })
      ;
  });
});
