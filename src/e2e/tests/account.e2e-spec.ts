import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AccountModule } from '../../modules/account/account.module';
import { AuthModule } from '../../modules/auth/auth.module';
import { E2eHelpersService } from '../helpers/e2e-helpers.service';
import { E2eHelpersModule } from '../helpers/e2e-helpers.module';
import { DemoAccountHelperFixture } from '../helpers/fixtures/demo-account.helper-fixture';

describe('account.e2e-spec.ts', () => {
  let app: INestApplication;
  let e2eHelpers: E2eHelpersService;

  beforeAll(async() => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        E2eHelpersModule,
        AccountModule,
        AuthModule,
      ],
    }).compile();

    e2eHelpers = module.get(E2eHelpersService);

    app = module.createNestApplication();

    await app.init();
  });

  beforeAll(async() => {
    await e2eHelpers.setUpTestingDatabase([
      DemoAccountHelperFixture,
    ]);
  });

  afterAll(async() => {
    await e2eHelpers.cleanUpTestingDatabase();

    await app.close();
  });

  it('/POST signup: new user with username "baz" will be create correctly', () => {
    return request(app.getHttpServer())
      .post('/signup')
      .send({ username: 'baz', password: '123456' })
      .set('Accept', 'application/json')
      .expect(200)
      .expect('Content-Type', /json/)
      .expect({
        success: true,
      })
      ;
  });

  it('/POST signup: new user with username "baz" will not be duplicated', () => {
    return request(app.getHttpServer())
      .post('/signup')
      .send({ username: 'baz', password: '123456' })
      .set('Accept', 'application/json')
      .expect(409)
      .expect('Content-Type', /json/)
      .expect({
        success: false,
        status: 409,
        message: 'Account with given username already exists',
      })
    ;
  });

  it('/POST login: "baz" is now available to login', () => {
    return request(app.getHttpServer())
      .post('/login')
      .send({ username: 'baz', password: '123456' })
      .set('Accept', 'application/json')
      .expect(200)
      .expect('Content-Type', /json/)
      .expect({
        success: true,
      })
    ;
  });
});
