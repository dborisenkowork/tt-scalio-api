import { Injectable } from '@nestjs/common';
import { AccountRepository } from '../../../modules/typeorm/repositories/account.repository';
import { AccountEntity } from '../../../modules/typeorm/entities/account.entity';
import { E2eFixture } from '../e2e-fixture';

@Injectable()
export class DemoAccountHelperFixture implements E2eFixture
{
  constructor(
    private readonly accountRepository: AccountRepository,
  ) {}

  async up(): Promise<void> {
    const requests: Array<{ username: string; password: string }> = [
      {username: 'foo', password: '123456'},
      {username: 'bar', password: '654321'},
    ];

    for (const request of requests) {
      const accountEntity: AccountEntity = new AccountEntity();

      accountEntity.username = request.username;
      accountEntity.password = request.password;

      await this.accountRepository.createAccount(accountEntity);
    }
  }
}
