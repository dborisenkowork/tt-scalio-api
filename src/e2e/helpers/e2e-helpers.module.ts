import { Module } from '@nestjs/common';
import { E2eHelpersService } from './e2e-helpers.service';
import { DemoAccountHelperFixture } from './fixtures/demo-account.helper-fixture';
import { TypeormModule } from '../../modules/typeorm/typeorm.module';

const e2eFixtures = [
  DemoAccountHelperFixture,
];

@Module({
  imports: [
    TypeormModule,
  ],
  providers: [
    E2eHelpersService,
    ...e2eFixtures,
  ],
  exports: [
    E2eHelpersService,
  ],
})
export class E2eHelpersModule
{}
