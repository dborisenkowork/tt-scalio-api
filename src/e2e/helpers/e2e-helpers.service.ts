import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { E2eFixture } from './e2e-fixture';
import { ModuleRef } from '@nestjs/core';

const sqlCleanUpScript = `
  DELETE FROM account;
`;

@Injectable()
export class E2eHelpersService
{
  constructor(
    private readonly connection: Connection,
    private readonly moduleRef: ModuleRef,
  ) {}

  async setUpTestingDatabase(fixtures: Array<Function> = []): Promise<void> {
    await this.cleanUpTestingDatabase();

    const fixturesToRun: Array<E2eFixture> = fixtures.map((f) => this.moduleRef.get(f as any));

    for (const fixture of fixturesToRun) {
      await fixture.up();
    }
  }

  async cleanUpTestingDatabase(): Promise<void> {
    const queryRunner = this.connection.createQueryRunner('master');

    await queryRunner.query(sqlCleanUpScript);
  }
}
