import { NestFactory } from '@nestjs/core';
import { FixturesModule } from './fixtures.module';

async function bootstrap(): Promise<any> {
  await NestFactory.createApplicationContext(FixturesModule);
}

bootstrap();
