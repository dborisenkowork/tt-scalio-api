import { Injectable } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { Fixture } from './fixture';
import { AccountsFixture } from './data-fixtures/accounts.fixture';

// tslint:disable:no-console

export const dataFixtures: Array<Function> = [
  AccountsFixture,
];

@Injectable()
export class FixturesService
{
  constructor(
    private readonly moduleRef: ModuleRef,
  ) {}

  async up(): Promise<void> {
    const fixtures: Array<Fixture> = dataFixtures.map((e) => this.moduleRef.get(e as any));

    for (const fixture of fixtures) {
      console.log(`[*] Running ${(fixture as any).constructor.name}`);

      await fixture.up();
    }
  }
}
