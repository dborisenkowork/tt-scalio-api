import { Injectable } from '@nestjs/common';
import { Fixture } from '../fixture';
import { AccountRepository } from '../../modules/typeorm/repositories/account.repository';
import * as faker from 'faker';
import { AccountEntity } from '../../modules/typeorm/entities/account.entity';

const MAX_DEMO_USERS: number = 10;
const DEFAULT_PASSWORD: string = '123456';

// tslint:disable:no-console

@Injectable()
export class AccountsFixture implements Fixture
{
  constructor(
    private readonly accountRepository: AccountRepository,
  ) {}

  async up(): Promise<void> {
    const requests: Array<{ username: string; password: string }> = [
      {username: 'foo', password: '123456'},
      {username: 'bar', password: '654321'},
    ];

    for (let i: number = 0; i < MAX_DEMO_USERS; i++) {
      requests.push({
        username: faker.internet.email(),
        password: DEFAULT_PASSWORD,
      });
    }

    // TOOD:: faker.js don't care about producing unique values. We should solve it somehow or just filter values.

    for (const request of requests) {
      const accountEntity: AccountEntity = new AccountEntity();

      accountEntity.username = request.username;
      accountEntity.password = request.password;

      await this.accountRepository.createAccount(accountEntity);
    }
  }
}
