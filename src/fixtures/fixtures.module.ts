import { Module, OnModuleInit } from '@nestjs/common';
import { dataFixtures, FixturesService } from './fixtures.service';
import { TypeormModule } from '../modules/typeorm/typeorm.module';

@Module({
  imports: [
    TypeormModule,
  ],
  providers: [
    FixturesService,
    ...(dataFixtures as any),
  ],
})
export class FixturesModule implements OnModuleInit {
  constructor(
    private readonly service: FixturesService,
  ) {}

  async onModuleInit(): Promise<void> {
    await this.service.up();
  }
}
