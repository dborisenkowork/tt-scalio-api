import { Body, Controller, HttpCode, HttpException, HttpStatus, Post } from '@nestjs/common';
import { AccountService } from '../../account/services/account.service';
import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import * as accountValidation from './../../account/account.validation-constants';
import { AccountNotFoundError } from '../../account/errors/account-not-found.error';
import { ServiceErrorResponse } from '../../../common/responses/service-error.response';
import { InvalidCredentialsError } from '../../account/errors/invalid-credentials.error';

export class AuthLoginRequest {
  @IsString()
  @IsNotEmpty()
  @MinLength(accountValidation.accountMinUsernameLength)
  @MaxLength(accountValidation.accountMaxUsernameLength)
  username: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(accountValidation.accountMinPasswordLength)
  @MaxLength(accountValidation.accountMaxPasswordLength)
  password: string;
}

export class AuthLoginResponse {
  success: boolean;
}

@Controller()
export class AuthController
{
  constructor(
    private readonly accountService: AccountService,
  ) {}

  @HttpCode(200)
  @Post('/login')
  async login(@Body() request: AuthLoginRequest): Promise<AuthLoginResponse> {
    try {
      await this.accountService.findAccountByUsernameAndPassword(
        request.username,
        request.password,
      );

      return {
        success: true,
      } as AuthLoginResponse;
    } catch (err) {
      if (err instanceof AccountNotFoundError) {
        throw new HttpException({
          status: HttpStatus.NOT_FOUND,
          success: false,
          message: 'Account with given username was not found',
        } as ServiceErrorResponse, HttpStatus.NOT_FOUND);
      } else if (err instanceof InvalidCredentialsError) {
        throw new HttpException({
          status: HttpStatus.FORBIDDEN,
          success: false,
          message: 'Invalid credentials',
        } as ServiceErrorResponse, HttpStatus.FORBIDDEN);
      } else {
        throw err;
      }
    }
  }
}
