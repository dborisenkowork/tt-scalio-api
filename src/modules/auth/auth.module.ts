import { Module } from '@nestjs/common';
import { AuthController } from './controllers/auth.controller';
import { AuthService } from './services/auth.service';
import { AccountModule } from '../account/account.module';

@Module({
  controllers: [
    AuthController,
  ],
  providers: [
    AuthService,
  ],
  imports: [
    AccountModule,
  ],
})
export class AuthModule
{}
