import { Body, Controller, HttpCode, HttpException, HttpStatus, Post } from '@nestjs/common';
import { AccountDuplicateError } from '../errors/account-duplicate.error';
import { ServiceErrorResponse } from '../../../common/responses/service-error.response';
import { AccountService } from '../services/account.service';
import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import * as accountValidation from '../account.validation-constants';

export class AuthSignUpRequest {
  @IsString()
  @IsNotEmpty()
  @MinLength(accountValidation.accountMinUsernameLength)
  @MaxLength(accountValidation.accountMaxUsernameLength)
  username: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(accountValidation.accountMinPasswordLength)
  @MaxLength(accountValidation.accountMaxPasswordLength)
  password: string;
}

export class AuthSignUpResponse {
  success: boolean;
}

@Controller()
export class AccountController
{
  constructor(
    private readonly accountService: AccountService,
  ) {}

  @HttpCode(200)
  @Post('/signup')
  async signUp(@Body() request: AuthSignUpRequest): Promise<AuthSignUpResponse> {
    try {
      await this.accountService.createNewAccount({
        username: request.username,
        password: request.password,
      });

      return {
        success: true,
      } as AuthSignUpResponse;
    } catch (err) {
      if (err instanceof AccountDuplicateError) {
        throw new HttpException({
          success: false,
          status: HttpStatus.CONFLICT,
          message: 'Account with given username already exists',
        } as ServiceErrorResponse, HttpStatus.CONFLICT);
      } else {
        throw err;
      }
    }
  }
}
