import { Module } from '@nestjs/common';
import { AccountService } from './services/account.service';
import { AccountController } from './controllers/account.controller';
import { TypeormModule } from '../typeorm/typeorm.module';

@Module({
  imports: [
    TypeormModule,
  ],
  controllers: [
    AccountController,
  ],
  providers: [
    AccountService,
  ],
  exports: [
    AccountService,
  ],
})
export class AccountModule
{}
