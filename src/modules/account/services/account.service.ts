import { Injectable } from '@nestjs/common';
import { AccountEntity } from '../../typeorm/entities/account.entity';
import { AccountNotFoundError } from '../errors/account-not-found.error';
import { IsNotEmpty, IsString, MaxLength, MinLength, validate, ValidationError } from 'class-validator';
import * as accountValidation from '../account.validation-constants';
import { AccountDuplicateError } from '../errors/account-duplicate.error';
import { InvalidCredentialsError } from '../errors/invalid-credentials.error';
import { AccountRepository } from '../../typeorm/repositories/account.repository';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';

export class CreateNewAccountRequest {
  @IsString()
  @IsNotEmpty()
  @MinLength(accountValidation.accountMinUsernameLength)
  @MaxLength(accountValidation.accountMaxUsernameLength)
  username: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(accountValidation.accountMinPasswordLength)
  @MaxLength(accountValidation.accountMaxPasswordLength)
  password: string;
}

@Injectable()
export class AccountService
{
  constructor(
    private readonly accountRepository: AccountRepository,
  ) {}

  async createNewAccount(request: CreateNewAccountRequest): Promise<AccountEntity> {
    const errors: Array<ValidationError> = await validate(request);

    request.username = request.username.trim();
    request.password = request.password.trim();

    if (errors.length) {
      throw new Error('Invalid CreateNewAccountRequest instance');
    }

    if (await this.accountRepository.hasAccountWithUsername(request.username)) {
      throw new AccountDuplicateError();
    }

    const accountEntity: AccountEntity = new AccountEntity();

    accountEntity.username = request.username;
    accountEntity.password = request.password;

    return this.accountRepository.createAccount(accountEntity);
  }

  async findAccountByUsernameAndPassword(username: string, password: string): Promise<AccountEntity> {
    try {
      const result: AccountEntity = await this.accountRepository.getAccountWithUsername(username);

      if (result.password !== password) {
        throw new InvalidCredentialsError();
      }

      return result;
    } catch (err) {
      if (err instanceof EntityNotFoundError) {
        throw new AccountNotFoundError();
      } else {
        throw err;
      }
    }
  }
}
