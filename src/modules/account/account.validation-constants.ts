export const accountMinUsernameLength: number = 3;
export const accountMaxUsernameLength: number = 32;

export const accountMinPasswordLength: number = 6;
export const accountMaxPasswordLength: number = 32;
