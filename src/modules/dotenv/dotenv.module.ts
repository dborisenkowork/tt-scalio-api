import { Module } from '@nestjs/common';
import { DotenvService } from './dotenv.service';

@Module({
  providers: [
    {
      provide: DotenvService,
      useValue: new DotenvService(__dirname + `/../../../env/${process.env.NODE_ENV || ''}.env`),
    },
  ],
  exports: [
    DotenvService,
  ],
})
export class DotenvModule
{}
