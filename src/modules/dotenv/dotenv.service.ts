import * as dotenv from 'dotenv';
import * as fs from 'fs';

export interface DotenvConfig {
  ACCESS_CONTROL_ALLOW_ORIGIN: string;
  ACCESS_CONTROL_ALLOW_HEADERS: string;
  ACCESS_CONTROL_ALLOW_METHODS: string;

  TYPEORM_CONNECTION: string;
  TYPEORM_HOST: string;
  TYPEORM_PORT: number;
  TYPEORM_USERNAME: string;
  TYPEORM_PASSWORD: string;
  TYPEORM_DATABASE: string;
}

export class DotenvService
{
  private readonly envConfig: DotenvConfig;

  constructor(filePath: string) {
    this.envConfig = dotenv.parse(fs.readFileSync(filePath)) as any;
  }

  get(key: keyof DotenvConfig): any {
    return this.envConfig[key];
  }
}
