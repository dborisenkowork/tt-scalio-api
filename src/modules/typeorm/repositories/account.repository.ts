import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { AccountEntity } from '../entities/account.entity';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';

@Injectable()
export class AccountRepository
{
  constructor(
    private readonly connection: Connection,
  ) {}

  getRepository(): Repository<AccountEntity> {
    return this.connection.getRepository(AccountEntity);
  }

  createAccount(accountEntity: AccountEntity): Promise<AccountEntity> {
    return this.getRepository().save(accountEntity);
  }

  async hasAccountWithUsername(username: string): Promise<boolean> {
    const result = await this.getRepository().find({
      where: [
        { username: username },
      ],
    });

    return result.length > 0;
  }

  async getAccountWithUsername(username: string): Promise<AccountEntity> {
    const result = await this.getRepository().find({
      where: [
        { username: username },
      ],
    });

    if (result.length === 0) {
      throw new EntityNotFoundError(AccountEntity, undefined);
    }

    return result[0];
  }
}
