import { Module, Provider } from '@nestjs/common';
import { DotenvService } from '../dotenv/dotenv.service';
import { TypeOrmModule as NestJsTypeOrmModule } from '@nestjs/typeorm';
import { DotenvModule } from '../dotenv/dotenv.module';
import { AccountRepository } from './repositories/account.repository';

const repositories: Array<Provider> = [
  AccountRepository,
];

@Module({
  imports: [
    NestJsTypeOrmModule.forRootAsync({
      imports: [
        DotenvModule,
      ],
      useFactory: async(configService: DotenvService) => ({
        type: configService.get('TYPEORM_CONNECTION') as any,
        host: configService.get('TYPEORM_HOST'),
        port: configService.get('TYPEORM_PORT'),
        username: configService.get('TYPEORM_USERNAME'),
        password: configService.get('TYPEORM_PASSWORD'),
        database: configService.get('TYPEORM_DATABASE'),
        entities: [__dirname + '/entities/*.entity{.ts,.js}'],
        synchronize: true,
      }),
      inject: [DotenvService],
    }),
  ],
  providers: [
    ...repositories,
  ],
  exports: [
    NestJsTypeOrmModule,
    ...repositories,
  ],
})
export class TypeormModule
{}
