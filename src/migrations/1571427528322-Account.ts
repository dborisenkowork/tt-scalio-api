import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Account1571427528322 implements MigrationInterface
{
    public async up(queryRunner: QueryRunner): Promise<any> {
      queryRunner.createTable(new Table({
        name: 'account',
        columns: [
          {
            name: 'username',
            type: 'varchar',
          },
          {
            name: 'password',
            type: 'password',
          },
        ],
      }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        queryRunner.dropTable('account');
    }
}
