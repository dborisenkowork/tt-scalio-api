import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { DotenvModule } from './modules/dotenv/dotenv.module';
import { DotenvService } from './modules/dotenv/dotenv.service';

async function bootstrap(): Promise<void> {
  const app: INestApplication = await NestFactory.create(AppModule, {
    cors: true,
  });

  app.useGlobalPipes(new ValidationPipe());

  await app.init();

  const dotenvService: DotenvService = app.select(DotenvModule).get(DotenvService);

  app.enableCors({
    origin: dotenvService.get('ACCESS_CONTROL_ALLOW_ORIGIN'),
    allowedHeaders: dotenvService.get('ACCESS_CONTROL_ALLOW_HEADERS'),
    methods: dotenvService.get('ACCESS_CONTROL_ALLOW_METHODS'),
  });

  await app.listen(3000);
}

bootstrap();
