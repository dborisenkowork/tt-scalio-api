# Test Assigment

## How to up project

1. `cp env/local.env env/.env`
2. `npm install`
3. `docker-compose up -d`
4. `npm run migrations:up` (up app database structure)
5. `npm run migrations:test:up` (up test database structure)
6. `npm run fixtures:up` (up fixtures for app database)
7. `npm run start` (for starting application), `npm run test` (for running tests)

Use these credentials to test application:

- username: `foo`, password: `123456`
- username: `bar`, password: `654321`

## Developer notes

- Every commit is stable, feel free to navigate between them (there is a version with in-memory repository and a version 
with pgsql database)
- `commitlint` and `husky` packages are used to lint commit messages and running tests before committing anything
- Directory structure  is created manually since I don't like code scaffolding
- Account and auth are separated
- E2E tests are available
- E2E tests are running against real testing database connection
